# insomnia-plugin-xdebug

Little **Insomnia** plugin that easily allows you to enable **Xdebug** debugging by adding an `XDEBUG_SESSION` cookie to the request.

To enable, simply check the `Xdebug` checkbox...  
Uncheck to disable.

![Sreenshot Xdebug plugin](./screenshot.png)

The `XDEBUG_SESSION`'s value has been set to `Insomnia`... This can easily be edited in `./src/plugin.js`.  
Most modern IDEs will accept any incoming connection on port 9000 (or another if configured otherwise) regardless of the value of the `XDEBUG_SESSION`.

Issues and suggestions can be submitted here:  
<https://gitlab.com/stoempdev/insomnia-plugin-xdebug/issues>

For any info about Insomnia, check out the website:  
<https://insomnia.rest/>

For Xdebug install, setup & configuration, check out the website:  
<https://xdebug.org/>

Some tools to debug:

- PHPStorm: <https://www.jetbrains.com/help/phpstorm/configuring-xdebug.html>
- Netbeans: <http://wiki.netbeans.org/HowToConfigureXDebug>
- Eclipse (and Zend Studio): <https://wiki.eclipse.org/Debugging_using_XDebug>
- Pugdebug: <http://pugdebug.com/>

